import 'package:flutter/material.dart';

const sPrimaryColor = Color(0xffff7643);
const sPrimaryLightColor = Color(0xffffecdf);
const sPrimaryGradientColor = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [Color(0xffffa53e), Color(0xffff7643)]);
const sSecondaryColor = Color(0xff979797);
const sTextColor = Color(0xff757575);

const sAnimationDuration = Duration(milliseconds: 200);
