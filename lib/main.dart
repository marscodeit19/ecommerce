import 'package:ecommerce/routes.dart';
import 'package:ecommerce/utils/constants.dart';
import 'package:ecommerce/view/screens/onboarding/onboarding.dart';
import 'package:flutter/material.dart';

void main () {
  return runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
        fontFamily: "Muli",
        textTheme: TextTheme(
          bodyText1: TextStyle(color: sTextColor),
          bodyText2: TextStyle(color: sTextColor),
        ),
      ),
      initialRoute: Routes.splashRoute,
     // home: Onboarding(),
    );
  }
}
