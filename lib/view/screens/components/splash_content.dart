import 'package:ecommerce/utils/constants.dart';
import 'package:ecommerce/utils/images.dart';
import 'package:ecommerce/utils/size_config.dart';
import 'package:flutter/material.dart';

class SplashContent extends StatelessWidget {
  const SplashContent({Key? key,  this.text,  this.image}) : super(key: key);
  final String? text;
  final String? image;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Spacer(),
        Text(text.toString(), textAlign: TextAlign.center),
        const Spacer(flex: 2,),
        Image.asset(image.toString(),
          height: getProportionateScreenHeight(265),
          width: getProportionateScreenWidth(235),
        ),
      ],
    );
  }
}
