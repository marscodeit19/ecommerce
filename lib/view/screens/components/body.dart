import 'package:ecommerce/utils/constants.dart';
import 'package:ecommerce/utils/images.dart';
import 'package:ecommerce/utils/size_config.dart';
import 'package:ecommerce/view/screens/components/defaultbutton.dart';
import 'package:ecommerce/view/screens/components/splash_content.dart';
import 'package:flutter/material.dart';

class Body extends StatefulWidget {
  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  int currentPage = 0;

  List<Map> splashData = [
    {
      "text": "Welcome to Flutter eCommerce, Let's shop",
      "image": Images.splash1,
    },
    {
      "text":
          "We help people conect with store \naround United State of America",
      "image": Images.splash2,
    },
    {
      "text": "We show the easy way to shop. \nJust stay at home with us",
      "image": Images.splash3,
    },
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Column(
          children: [
            SizedBox(
              height: getProportionateScreenHeight(40),
            ),
            Text(
              "Flutter eCommerce",
              style: TextStyle(
                fontSize: getProportionateScreenWidth(36),
                fontWeight: FontWeight.bold,
                color: sPrimaryColor,
              ),
            ),
            Expanded(
                flex: 3,
                child: PageView.builder(
                  itemCount: splashData.length,
                  onPageChanged: ((value) {
                    setState(() {
                      currentPage = value;
                    });
                  }),
                  itemBuilder: (context, index) => SplashContent(
                    text: splashData[index]["text"],
                    image: splashData[index]["image"],
                  ),
                )),
            Expanded(
              flex: 2,
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20)),
                child: Column(
                  children: [
                    Spacer(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(
                        splashData.length,
                        (index) => buildDot(index: index),
                      ),
                    ),
                    Spacer(
                      flex: 3,
                    ),
                    DefaultButton(
                      text: currentPage == 2 ? "Continue" : "Next",
                      press: () {
                      },
                    ),
                    Spacer(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  AnimatedContainer buildDot({int? index}) {
    return AnimatedContainer(
      duration: sAnimationDuration,
      height: 6,
      width: currentPage == index ? 20 : 6,
      margin: EdgeInsets.only(right: 5),
      decoration: BoxDecoration(
        color: currentPage == index ? sPrimaryColor : Color(0xffd8d8d8),
        borderRadius: BorderRadius.circular(50),
      ),
    );
  }
}
